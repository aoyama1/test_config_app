class AboutController < ApplicationController
  def index
  end

  def result
    lang = params[:lang].presence || 'ja'
    if lang == 'ja'
      I18n.default_locale = :ja
    else
      I18n.default_locale = :en
    end
  end
end
